import pycurl
import time
import re
import argparse
from io import BytesIO
from bs4 import BeautifulSoup
from urllib.parse import urljoin

HTTP = "http://"
HTTPS = "https://"
DEBUG = False

# Utility class to get HTTP headers easily
# Only what is used is implemented
class Header:
	def __init__(self):
		self.headers = {}

	def parse(self, header_line):
		header_line = header_line.decode('iso-8859-1')

		if ':' not in header_line:
			return

		name, value = header_line.split(':', 1)

		name = name.strip()
		value = value.strip()
		name = name.lower()

		self.headers[name] = value

	def encoding(self):
		encoding = None
		if 'content-type' in self.headers:
			content_type = self.headers['content-type'].lower()
			match = re.search('charset=(\S+)', content_type)
			if match:
				encoding = match.group(1)
				return encoding
		if encoding is None:
			return 'iso-8859-1'

	def mime_type(self):
		mime_type = None
		if 'content-type' in self.headers:
			content_type = self.headers['content-type'].lower()
			mime_type = content_type.split(';')[0].strip()
		return mime_type

# The actual mapper. Calling recursive_scraper will begin to
# scrape the website
class Mapper:

	def __init__(self):
		self.map = {}
		self.pages_found = []
		self.visited = []
		self.errors = []

	def recursive_scraper(self, url):
		# If any errors occur do not stop scraping
		# just log them and continue
		try:
			if DEBUG: print("accessing: " + url)
			url = self.strip_protocol(url)
			self.visited.append(url)

			buffer = BytesIO()
			header = Header()
			c = pycurl.Curl()
			#c.setopt(c.VERBOSE, True)
			c.setopt(c.URL, HTTP + url)
			c.setopt(c.WRITEDATA, buffer)
			c.setopt(c.HEADERFUNCTION, header.parse)
			c.perform()
			c.close()
			mime_type = header.mime_type()
			if mime_type is None or "html" not in mime_type: 
				return
			body = buffer.getvalue()
			body = body.decode(header.encoding())

			soup = BeautifulSoup(body, 'html.parser')

			if DEBUG: print("success")
			if DEBUG: print("finding links")

			for link in soup.find_all('a', href=True):
				href = link.get('href')
				if href is '' or href.startswith('#'):
					continue
				# fix issues with links formatted like: /path/to/file
				# urljoin does not work without the protocol
				href = urljoin(HTTP + url, href)
				href = self.strip_protocol(href)

				if DEBUG: print("found:" + href)
				
				if args.website in href:
					if href not in self.visited:
						time.sleep(args.delay)
						self.recursive_scraper(href)
				if any(link_to in href for link_to in args.links_to):
					if not (args.ignore_repeats and href in self.pages_found) and href != url:
						href_type = self.request_mime_type(href)
						if href_type is not None and "html" in href_type:
							self.pages_found.append(href)
							if url in self.map and href not in self.map[url]:
								self.map[url].append(href)
							else:
								self.map[url] = [href,]
		except Exception as e:
			self.errors.append("There was an error with " + url +"\n    "+ repr(e))

	def request_mime_type(self, url):
		header = Header()
		c = pycurl.Curl()
		c.setopt(c.URL, url)
		c.setopt(c.NOBODY, True)
		c.setopt(c.HEADERFUNCTION, header.parse)
		c.perform()
		c.close()
		return header.mime_type()

	def strip_protocol(self, url):
		if (url.startswith("https://")):
			url = url[8:]
		if (url.startswith("http://")):
			url = url[7:]
		return url

	def print_map(self):
		print("\n")
		for key in sorted(self.map.keys()):
			print(key)
			for item in self.map[key]:
				print("    " + item)

	def print_errors(self):
		for error in self.errors:
			print(error)

	def print_list(self):
		for item in self.pages_found:
			print(item)

# Parse arguments
parser = argparse.ArgumentParser(description="Map a website.")

parser.add_argument("website", help="The website to map. Must not include http:// or https://")
parser.add_argument("links_to", default=[], nargs="*", help="If this is specified \"website\" will be scanned for any links leading to something in this list")

parser.add_argument("-f", "--fast", action='store_true', help="Maps the website as fast as it can. This will cause a lot of traffic to the website. DO NOT use unless you have permission from the site manager. Equivalent to -d 0")
parser.add_argument("-d", "--delay", type=float, default=0.25, help="The delay to use in between requests. Default is 0.25 (One quarter of a second)")
parser.add_argument("-i", "--ignore_repeats", action="store_true", help="This will ignore any repeats in the site. Only unique urls will be kept")
parser.add_argument("-l", "--list", action="store_true", help="This will create a list of all pages under the domain. -i is implied")
parser.add_argument("-p", "--pages_only", action="store_true", help="Do not include files (such as pdf or png). If this is used then extra head requests will be made. These requests will not be affected by delay")

args = parser.parse_args()

if args.fast:
	args.delay = 0

if len(args.links_to) == 0:
	args.links_to.append(args.website)

if args.list:
	args.ignore_repeats = True

# Start the mapping
# print map and errors when finished
mapper = Mapper()
mapper.recursive_scraper(args.website)
if (args.list):
	mapper.print_list()
else:
	mapper.print_map()
mapper.print_errors()